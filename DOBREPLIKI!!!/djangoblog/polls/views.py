from django.http import HttpResponse
from django.shortcuts import render
from models import Poll
from models import Choice, Article
from django.template import Context, loader

def index(request):
    latest_poll_list = Article.objects.all()
    context = {'latest_poll_list': latest_poll_list}
    return render(request, 'polls/index.html', context)

def detail(request, poll_id):
    result = Choice.objects.all(pk=poll_id)
    context = {'result': result}
    return render(request, 'polls/details.html', context)

def results(request, poll_id):
    result = Choice.objects.all(pk=poll_id)
    context = {'result': result}
    return render(request, 'polls/details.html', context)

def vote(request, poll_id):
    return HttpResponse("You're voting on poll %s." % poll_id)