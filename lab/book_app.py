# -*- coding: utf-8 -*-

from flask import Flask
import bookdb

app = Flask(__name__)
app.config['DEBUG'] = True

db = bookdb.BookDB()


@app.route('/')
def books():
    # Przekaż listę książek do szablonu "book_list.html"
    pass


@app.route('/book/<book_id>/')
def book(book_id):
    # Przekaż szczegóły danej książki do szablonu "book_detail.html"
    pass


if __name__ == '__main__':
    app.run(debug=True)